import React, {Component, useEffect, useRef, useState} from "react";
import {SafeAreaView, StyleSheet, Text, View} from "react-native";
import {Clock, concat, Transition, Transitioning} from "react-native-reanimated";
import Animated, {Easing} from 'react-native-reanimated';
import _ from 'lodash'
import {TouchableOpacity} from "react-native-gesture-handler";

const {timing, spring, color, debug, set, Value, block, startClock, clockRunning, cond, stopClock} = Animated;

function runTiming(clock, value, dest) {
    const state = {
        finished: new Value(0),
        position: new Value(0),
        time: new Value(0),
        frameTime: new Value(0),
    };

    const config = {
        duration: 400,
        toValue: new Value(0),
        easing: Easing.inOut(Easing.ease),
    };

    return block([
        cond(
            clockRunning(clock),
            [
                // if the clock is already running we update the toValue, in case a new dest has been passed in
                set(config.toValue, dest),
            ],
            [
                // if the clock isn't running we reset all the animation params and start the clock
                set(state.finished, 0),
                set(state.time, 0),
                set(state.position, value),
                set(state.frameTime, 0),
                set(config.toValue, dest),
                startClock(clock),
            ]
        ),
        // we run the step here that is going to update position
        timing(clock, state, config),
        // if the animation is over we stop the clock
        cond(state.finished, stopClock(clock)),
        state.position,
    ]);
}

function runTimingV1(clock, value, dest) {
    const state = {
        finished: new Value(0),
        position: new Value(0),
        time: new Value(0),
        frameTime: new Value(0),
    };

    const config = {
        duration: 400,
        toValue: new Value(0),
        easing: Easing.inOut(Easing.ease),
    };

    return block([
        cond(
            clockRunning(clock),
            [
                // if the clock is already running we update the toValue, in case a new dest has been passed in
                set(config.toValue, dest),
            ],
            [
                // if the clock isn't running we reset all the animation params and start the clock
                set(state.finished, 0),
                set(state.time, 0),
                set(state.position, value),
                set(state.frameTime, 0),
                set(config.toValue, dest),
                startClock(clock),
            ]
        ),
        // we run the step here that is going to update position
        timing(clock, state, config),
        // if the animation is over we stop the clock
        cond(state.finished, stopClock(clock)),
        state.position,
    ]);
}


class OrderItem extends Component {

    constructor(props) {
        super(props);
        this.v = new Value(this.props.percent)
        this.v1 = new Value(1)

        this.clock = new Clock()
        this.clockText = new Clock()
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.percent !== this.props.percent) {
            // spring(this.v, {
            //     toValue: nextProps.percent,
            //     damping: 2,
            //     mass: 1,
            //     stiffness: 121.6,
            //     overshootClamping: false,
            //     restSpeedThreshold: 0.001,
            //     restDisplacementThreshold: 0.001,
            // }).start()
            // timing(this.v, {
            //     duration: 600,
            //     toValue: nextProps.percent,
            //     easing: Easing.inOut(Easing.ease),
            // }).start()
            this.v = runTiming(this.clock, this.props.percent, nextProps.percent)
        }

        if (nextProps.value !== this.props.value) {
            this.v1 = runTimingV1(this.clockText, 20, 0)
            // this.v2 = runTiming(this.clockView, 0.6, 1)
        }
    }

    render() {
        // console.log(this.v, typeof this.v)
        return (
            <TouchableOpacity onPress={() => console.log(Math.random())}>
                <Animated.View style={[styles.orderItemContainer]}>
                    <Animated.View
                        style={[
                            styles.backgroundOrderItem, this.props.type !== 'buy' ? {
                                left: 0,
                                backgroundColor: 'red'
                            } : {right: 0, backgroundColor: 'green'},
                            {
                                width: concat(this.v, "%")
                            }
                        ]}/>
                    <Animated.View style={[styles.contentOrderItem, { transform: [{ translateY: this.v1 }] }]}>
                        <Text>{this.props.amount}</Text>
                        <Text>{this.props.value}</Text>
                    </Animated.View>
                </Animated.View>
            </TouchableOpacity>
        )
    }
}

function OrderBook() {
    const [a1, setA1] = useState([])
    const [a2, setA2] = useState([])

    useEffect(() => {
        setInterval(() => {
            setA1(_.sortBy(Array.from({length: 20}, () => ({
                value: Math.random().toFixed(8),
                amount: Math.random().toFixed(8),
                percent: Math.random() * 100,
            })), e => e.percent))
            setA2(_.sortBy(Array.from({length: 20}, () => ({
                value: Math.random().toFixed(8),
                amount: Math.random().toFixed(8),
                percent: Math.random() * 100,
            })), e => e.percent))
        }, 5000)
    }, [])

    return (
        <View style={styles.container}>
            {a1.length > 0 &&
            <View style={{display: 'flex', flexDirection: 'column', flex: 1, marginRight: 3}}>
                {/*{a1.map((e) => <OrderItem type={'buy'} {...e} key={e.value}/>)}*/}
                <OrderItem type={'buy'} {...a1[0]}/>
                <OrderItem type={'buy'} {...a1[1]}/>
                <OrderItem type={'buy'} {...a1[2]}/>
                <OrderItem type={'buy'} {...a1[3]}/>
                <OrderItem type={'buy'} {...a1[4]}/>
                <OrderItem type={'buy'} {...a1[5]}/>
                <OrderItem type={'buy'} {...a1[6]}/>
                <OrderItem type={'buy'} {...a1[7]}/>
            </View>}
            {/*{a2.length > 0 && <Animated.View style={{display: 'flex', flexDirection: 'column', flex: 1, marginLeft: 3}}>*/}
            {/*    /!*{a2.map((e) => <OrderItem type={'sell'} {...e} key={e.value}/>)}*!/*/}
            {a2.length > 0 && <View style={{display: 'flex', flexDirection: 'column', flex: 1, marginLeft: 3}}>
                <OrderItem type={'sell'} {...a2[0]}/>
                <OrderItem type={'sell'} {...a2[1]}/>
                <OrderItem type={'sell'} {...a2[2]}/>
                <OrderItem type={'sell'} {...a2[3]}/>
                <OrderItem type={'sell'} {...a2[4]}/>
                <OrderItem type={'sell'} {...a2[5]}/>
                <OrderItem type={'sell'} {...a2[6]}/>
                <OrderItem type={'sell'} {...a2[7]}/>
            </View>}
            {/*</Animated.View>}*/}
        </View>
    )
}

export default OrderBook

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginTop: 100
    },
    orderItemContainer: {
        width: '100%',
        height: 20,
        marginBottom: 5,
        overflow: 'hidden',
    },
    backgroundOrderItem: {
        position: 'absolute',
        right: 0,
        height: '100%',
        width: '100%',
        backgroundColor: 'green',
    },
    contentOrderItem: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }
})
