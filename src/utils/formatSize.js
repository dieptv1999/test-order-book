//iphone 8
const baseWidth = 375;
const baseHeight = 812;

import { Dimensions, Platform } from 'react-native';

const { height, width } = Dimensions.get('window');
const isPad = Platform.isPad
// padding, margin, fontSize, ....
export const scale = size => (width / baseWidth * size) >> 0;

// width
export const wScale = size => (height / baseHeight * size) * (isPad ? 1.1 : 1) >> 0;

// height
export const hScale = (size, factor = 0.5) => (size + (scale(size) - size) * factor) >> 0;

// Setting AutoScaling
export const fontScaling = false
