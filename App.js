import * as React from "react";
import OrderBook from "./src/components/OrderBook";
import {GestureHandlerRootView} from "react-native-gesture-handler";

export default function App() {
    return (
        <GestureHandlerRootView style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
        }}>
            <OrderBook/>
            {/*<AnimatedBox/>*/}
        </GestureHandlerRootView>
    );
}
